package backend

import (
	"errors"
	"sync"
	"time"
)

type InMemStore struct {
	sync.RWMutex
	table []string
}

func NewInMemStore() *InMemStore {
	return &InMemStore{table: make([]string, 0)}
}

func (m *InMemStore) Store(url string, timestamp time.Time) (int64, error) {
	m.Lock()
	defer m.Unlock()

	m.table = append(m.table, url)
	return int64(len(m.table) - 1), nil
}

func (m *InMemStore) Fetch(index int64) (string, error) {
	m.RLock()
	defer m.RUnlock()

	var err error
	var url string

	idx := int(index)

	if idx > 0 && idx < len(m.table) {
		url = m.table[idx]
	} else {
		err = errors.New("Index out of range")
	}

	return url, err
}
