package main

import (
	"bitbucket.org/simonhartmann/router"
	"bitbucket.org/simonhartmann/shorty/backend"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"
)

var port = *flag.Int("port", 8080, "set api port")
var host = *flag.String("host", "127.0.0.1", "set host")

var (
	httpRouter *router.Router
	mux        *http.ServeMux
	store      DataStore
)

type DataStore interface {
	Store(url string, timestamp time.Time) (int64, error)
	Fetch(index int64) (string, error)
}

func main() {

	store = backend.NewInMemStore()

	mux = http.NewServeMux()
	mux.Handle("/", http.FileServer(http.Dir("www")))

	httpRouter = router.NewRouter()
	httpRouter.Get("/shorty/{shortcut}", http.HandlerFunc(getURL))
	httpRouter.Post("/shorty/", http.HandlerFunc(createShortcut))
	mux.Handle("/shorty/", http.HandlerFunc(httpRouter.ServeHTTP))

	addr := fmt.Sprintf("%s:%v", host, port)
	log.Printf("Running shorty at %s\n", addr)

	go http.ListenAndServe(addr, mux)

	sigchan := make(chan os.Signal, 10)
	signal.Notify(sigchan, os.Interrupt)
	<-sigchan

	log.Println("Stopping shorty.")
}

func test(w http.ResponseWriter, r *http.Request) {

}

func getURL(w http.ResponseWriter, r *http.Request) {

	shortcut := router.GetParam(r, "shortcut")

	if len(shortcut) <= 0 {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	var (
		err error
		idx int64
		url string
	)

	idx, err = strconv.ParseInt(shortcut, 36, 64)
	url, err = store.Fetch(idx)

	if err != nil {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}

	http.Redirect(w, r, url, http.StatusFound)
}

func createShortcut(w http.ResponseWriter, r *http.Request) {

	url := r.FormValue("url")

	if len(url) <= 0 {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	// todo: validate url?

	timestamp := time.Now().UTC()

	idx, err := store.Store(url, timestamp)
	shortcut := strconv.FormatInt(idx, 36)

	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	surl := fmt.Sprintf("%s/%s", host, shortcut)
	log.Printf(surl)

	if body, err := json.Marshal([]byte(surl)); err == nil {
		w.Write(body)
	}

	w.Header().Add("content-type", "application/json")
}
